<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");
include_once(__DIR__ . "/../source/Huntsman/Download.php");

$Snapshot = new Huntsman\Snapshot();
$Download = new Huntsman\Download();
$galleryId = null;


if (isset($_GET["gallery"])){
  $galleryId = $_GET["gallery"];
  $gallery = $Snapshot->getAll($galleryId);
  
  $zip = $Download->createZIP($gallery, $galleryId);
  
  header('Content-Type: application/zip');
  header("Content-Disposition: attachment; filename='" . basename($zip) . "'");
  header('Content-Length: ' . filesize($zip));
  readfile($zip);
}