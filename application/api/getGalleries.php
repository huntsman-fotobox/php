<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Gallery.php");

$Gallery = new \Huntsman\Gallery();

$single = null;
$limit = null;

if (isset($_GET["limit"])){
  $limit = $_GET["limit"];
}

if (isset($_GET["id"])){
  $single = $_GET["id"];
  echo json_encode($Gallery->getSingle($single), true);
} else{
  echo json_encode($Gallery->getAll($limit), true);
}