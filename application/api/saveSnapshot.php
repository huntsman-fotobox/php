<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");


if (isset($_FILES) && $_SERVER["REQUEST_METHOD"] === "POST"){
  $Snapshot = new Huntsman\Snapshot();
  
  $d = new DateTime();
  $uploadFolderPath = "./../_uploads/";
  
  $fileName = $_FILES["image"]["name"];
  $fileTmpName = $_FILES["image"]["tmp_name"];
  $fileSize = $_FILES["image"]["size"];
  $fileError = $_FILES["image"]["error"];
  $fileType = $_FILES["image"]["type"];
  
  $fileExt = explode('/', $fileType);
  $currentFileType = strtolower(end($fileExt));
  
  $currentFolder = $d->format('Y-m-d') . "/";
  $id = $d->getTimestamp();
  
  $galleryId = $_SERVER["HTTP_GALLERY_ID"];
  $filter = $_SERVER["HTTP_FILTER"];
  
  $snapshotData = array(
    "error" => $fileError,
    "fileName" => $id . "." . $currentFileType,
    "filter" => $filter,
    "folderName" => $currentFolder,
    "gallery" => $galleryId,
    "id" => $id,
    "size" => $fileSize,
    "tmpName" => $fileTmpName,
    "type" => $fileType,
  );
  
  $Snapshot->storeSnapshot($snapshotData);
  $Snapshot->saveSnapshot($snapshotData);
  
  echo json_encode($snapshotData, true);
  
} else {
  echo json_encode(array(), true);
}




