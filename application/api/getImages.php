<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");

$Snapshot = new Huntsman\Snapshot();

$single = null;
$limit = null;
$gallery = null;

if (isset($_GET["limit"])){
  $limit = $_GET["limit"];
}

if (isset($_GET["gallery"])){
  $gallery = $_GET["gallery"];
}

if (isset($_GET["id"])){
  $single = $_GET["id"];
  echo json_encode($Snapshot->getSingle($single), true);
} else{
  echo json_encode($Snapshot->getAll($gallery, $limit), true);
}