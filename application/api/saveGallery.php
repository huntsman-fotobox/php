<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Gallery.php");


if ($_SERVER["REQUEST_METHOD"] === "POST" && $_SERVER["HTTP_GALLERY_SLUG"]){
  $Gallery = new Huntsman\Gallery();
  
  $d = new DateTime();
  
  $galleryItem = array(
    "id" => uniqid(),
    "slug" => $_SERVER["HTTP_GALLERY_SLUG"],
    "label" => $_SERVER["HTTP_GALLERY_LABEL"],
    "create" => $d->format('Y-m-d H:i:s')
  );
  
  $Gallery->saveGallery($galleryItem);
  
  echo json_encode($galleryItem, true);
} else {
  echo json_encode(array(), true);
}




