<?php
include_once(__DIR__ . "/../includes/header.php");
include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");

$Snapshot = new Huntsman\Snapshot();

if (isset($_SERVER["HTTP_IMAGE_ID"])){
  $imageId = $_SERVER["HTTP_IMAGE_ID"];
  echo json_encode($Snapshot->remove($imageId), true);
}