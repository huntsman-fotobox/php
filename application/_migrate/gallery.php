<?php

include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");
include_once(__DIR__ . "/../source/Huntsman/Gallery.php");

$Snapshot = new Huntsman\Snapshot();
$Gallery = new Huntsman\Gallery();

$allPhotos = $Snapshot->getAll();
$galleries = array();

foreach ($allPhotos["photos"] as $image) {
  if(!isset($galleries[$image["gallery"]])){
    $galleries[$image["gallery"]] = array(
      "id" => $image["gallery"],
      "slug" => $image["gallery"],
      "label" => $image["gallery"],
      "create" => date("Y-m-d H:i:s", $image["id"])
    );
  }
}

foreach ($galleries as $galleryItem) {
  echo $Gallery->saveGallery($galleryItem);
}

echo "<br /><strong>saved!</strong>";