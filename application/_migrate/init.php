<?php

include_once(__DIR__ . "/../source/Huntsman/Snapshot.php");

$Snapshot = new Huntsman\Snapshot();

function getFileDetails($dir, $filename){
  $image = explode(".", $filename);
  $folder = explode(__DIR__ . "/../_uploads", $dir);
  
  return array(
    "error" => 0,
    "fileName" => $filename,
    "filter" => null,
    "folderName" => ltrim($folder[1], DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
    "gallery" => ltrim($folder[1], DIRECTORY_SEPARATOR),
    "id" => intval($image[0]),
    "size" => null,
    "tmpName" => null,
    "type" => "image/" . $image[1]
  );
}

function scanFolder($dir){
  $excludeFolder = array("..", ".", ".DS_Store");
  
  $folderData = array_diff(scandir($dir), $excludeFolder);
  $output = array();
  
  foreach ($folderData as $key => $item){
    if (strpos($item, '.') !== false){
      $output[] = getFileDetails($dir, $item);
    } else{
      $output = $folderData;
    }
  }
  
  rsort($output);
  return $output;
}

function getImages(){
  $result = array();
  
  $cdir = scanFolder(__DIR__ . "/../_uploads");
  foreach ($cdir as $key => $value){
    if (is_dir(__DIR__ . "/../_uploads" . DIRECTORY_SEPARATOR . $value)){
      $folderContent = scanFolder(__DIR__ . "/../_uploads" . DIRECTORY_SEPARATOR . $value);
      
      array_push($result, array("name" => $value, "images" => $folderContent));
    } else{
      $result[] = $value; // ?
    }
  }
  
  return $result;
}

$allImages = getImages();
foreach ($allImages as $folder) {
  foreach ($folder["images"] as $image) {
    $Snapshot->saveSnapshot($image);
  }
}

