<?php

namespace Huntsman;

interface Database{
  
  /**
   * @param $document
   * @return mixed
   */
  public function save($document);
  
  
  /**
   * @param $filter
   * @return mixed
   */
  public function getOne($filter);
  
  
  /**
   * @param $galleryId
   * @param $limit
   * @return mixed
   */
  public function getImages($galleryId, $limit);
  
  
  /**
   * @param $imageId
   * @return mixed
   */
  public function removeImage($imageId);
}