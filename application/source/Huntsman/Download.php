<?php

namespace Huntsman;

require __DIR__ . '/../../vendor/autoload.php';

use Twig_Loader_Filesystem;
use Twig_Environment;

class Download{
  const TEMPLATE_PATH = __DIR__ . "/../../includes/templates";
  const CACHE_PATH = __DIR__ . "/../../includes/cache";
  const UPLOAD_FOLDER = __DIR__ . "/../../_uploads" . DIRECTORY_SEPARATOR;
  
  public function __construct(){
    $loader = new Twig_Loader_Filesystem(self::TEMPLATE_PATH);
    $this->twig = new Twig_Environment($loader, array('debug' => true, 'cache' => self::CACHE_PATH,));
  }
  
  
  /**
   * @param $gallery
   * @param $gallerySlug
   * @return string
   * @throws \Twig_Error_Loader
   * @throws \Twig_Error_Runtime
   * @throws \Twig_Error_Syntax
   */
  public function createZIP($gallery, $gallerySlug){
    $photos = $gallery["photos"];
    $zipname = self::CACHE_PATH . DIRECTORY_SEPARATOR . $gallerySlug . ".zip";
    
    $zip = new \ZipArchive();
    $zip->open($zipname, \ZipArchive::CREATE);
    
    // add images
    foreach ($photos as $photo){
      $photoOrigin = self::UPLOAD_FOLDER . $photo["folderName"] . $photo["fileName"];
      $zip->addFile($photoOrigin, "/photos/" . $photo["fileName"]);
    }
    
    // add overview
    $htmlContent = $this->renderTemplate($gallery);
    $html = $this->generateHTML($htmlContent);
    $zip->addFile($html, "/index.html");
    
    $zip->close();
    
    return $zipname;
  }
  
  
  /**
   * @param $gallery
   * @return mixed
   * @throws \Twig_Error_Loader
   * @throws \Twig_Error_Runtime
   * @throws \Twig_Error_Syntax
   */
  private function renderTemplate($gallery){
    return $this->twig->render("download.html", ["gallery" => $gallery]);
  }
  
  
  /**
   * @param $content
   * @return string
   */
  private function generateHTML($content){
    $htmlname = self::CACHE_PATH . "/gallery-cache.html";
    
    $file = fopen($htmlname, "w") or die("Unable to open file!");
    fwrite($file, $content);
    fclose($file);
    
    return $htmlname;
  }
}