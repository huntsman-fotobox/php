<?php

namespace Huntsman;

include_once(__DIR__ . '/File.php');

class ScanFolder implements Database{
  
  const UPLOAD_FOLDER = __DIR__ . "/../../_uploads";
  
  protected $file;
  
  public function __construct(){
    $this->file = new File();
  }
  
  /**
   * @param $dir
   * @return array
   */
  private function scanFolder($dir){
    $excludeFolder = array("..", ".", ".DS_Store");
    
    $folderData = array_diff(scandir($dir), $excludeFolder);
    $output = array();
    
    foreach ($folderData as $key => $item){
      if (strpos($item, '.') !== false){
        $output[] = $this->file->getFileDetails($dir, $item);
      } else{
        $output = $folderData;
      }
    }
    
    rsort($output);
    return $output;
  }
  
  /**
   * @param $document
   * @return mixed
   */
  public function save($document){
    return;
  }
  
  
  /**
   * @param $filter
   * @return array|mixed
   */
  public function getOne($filter){
    $result = array();
    
    $cdir = $this->scanFolder(self::UPLOAD_FOLDER);
    foreach ($cdir as $key => $value){
      if (is_dir(self::UPLOAD_FOLDER . DIRECTORY_SEPARATOR . $value)){
        $folderContent = $this->scanFolder(self::UPLOAD_FOLDER . DIRECTORY_SEPARATOR . $value);
        
        foreach ($folderContent as $file){
          if ($file["name"] == $filter){
            $result = $file;
          }
        }
      } else{
        if ($value["name"] == $filter){
          $result = $value;
        }
      }
    }
    
    return json_encode($result, true);
  }
  
  
  /**
   * @param $galleryId
   * @param $limit
   * @return false|mixed|string
   */
  public function getImages($galleryId, $limit){
    $result = array();
  
    $cdir = $this->scanFolder(self::UPLOAD_FOLDER);
    foreach ($cdir as $key => $value){
      if (is_dir(self::UPLOAD_FOLDER . DIRECTORY_SEPARATOR . $value)){
        $folderContent = $this->scanFolder(self::UPLOAD_FOLDER . DIRECTORY_SEPARATOR . $value);
      
        if ($galleryId){
          $result = array_merge($result, $folderContent);
        } else{
          array_push($result, array("name" => $value, "images" => $folderContent));
        }
      
      } else{
        $result[] = $value; // ?
      }
    }
  
    if ($limit){
      $result = array_slice($result, 0, $limit);
    }
  
    return json_encode($result, true);
  }
  
  
  /**
   * @param $imageId
   * @return mixed|void
   */
  public function removeImage($imageId){
    return;
  }
}