<?php

namespace Huntsman;

class File{
  
  const UPLOAD_FOLDER = __DIR__ . "/../../_uploads";
  
  
  /**
   * @param $dir
   * @param $filename
   * @return array
   */
  public function getFileDetails($dir, $filename){
    $image = explode(".", $filename);
    $folder = explode(self::UPLOAD_FOLDER, $dir);
    
    return array(
      "parent" => ltrim($folder[1], DIRECTORY_SEPARATOR),
      "name" => $image[0],
      "type" => $image[1],
      "fileName" => $filename,
      "url" => $folder[1] . DIRECTORY_SEPARATOR . $filename
    );
  }
  
  
  /**
   * @param $file
   * @return bool
   */
  public function deleteFile($file){
    if (file_exists($file)){
      if (@unlink($file) !== true)
        throw new Exception('Could not delete file: ' . $file . ' Please close all applications that are using it.');
    }
    
    return true;
  }
}