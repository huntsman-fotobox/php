<?php

namespace Huntsman;
include_once('Database.php');

use MongoLite\Client;

require __DIR__ . '/../../vendor/autoload.php';

class MongoLite implements Database{
  const DATABASE_PATH = __DIR__ . '/../../_database';
  
  private $table;
  private $collection;
  
  public function __construct($table){
    $this->table = $table;
    
    $client = new Client(self::DATABASE_PATH);
    $database = $client->HUNTSMAN;
    
    $this->collection = array(
      "photos" => $database->photos,
      "gallery" => $database->gallery
    );
  }
  
  
  /**
   * @param $json
   * @return mixed
   */
  public function save($json){
    $document = json_encode($json);
  
    return $this->collection[$this->table]->insert(
      json_decode($document, true)
    );
  }
  
  
  /**
   * @param $id
   * @param null $table
   * @return false|mixed|string
   */
  public function getOne($id, $table = null){
    $output = array();
  
    if(isset($table)) {
      $collectionType = $table;
    } else {
      $collectionType = $this->table;
    }
    
    $response = $this->collection[$collectionType]->findOne([
      "id" => $id
    ]);
  
    if ($response) {
      $output = $response;
    }

    return $output;
  }
  
  
  /**
   * @param $galleryId
   * @param $limit
   * @return false|mixed|string
   */
  public function getImages($galleryId, $limit){
    $output = array(
      "info" => $this->getOne($galleryId, 'gallery'),
      "photos" => array()
    );
  
    $response = $this->collection[$this->table]->find(function ($document) use ($galleryId){
      if ($galleryId){
        return $document["gallery"] == $galleryId;
      } else{
        return $document["_id"];
      }
    });
  
    if ($response->count() > 0){
      foreach ($response as $item){
        $output["photos"][] = $item;
      }
    
      $output["photos"] = array_reverse($output["photos"]);
    
      if ($limit){
        $output["photos"] = array_slice($output["photos"], 0, $limit);
      }
    }
  
    return $output;
  }
  
  
  /**
   * @param $limit
   * @return false|string
   */
  public function getGalleries($limit){
    $output = array();
    
    $response = $this->collection[$this->table]->find(function ($document){
      return $document["create"];
    });
    
    foreach ($response as $item){
      $output[] = $item;
    }
  
    $output = array_reverse($output);
    
    if ($limit){
      $output = array_slice($output, 0, $limit);
    }
    
    return $output;
  }
  
  
  /**
   * @param $imageId
   * @return mixed
   */
  public function removeImage($imageId){
    return $this->collection[$this->table]->remove(["id" => $imageId]);
  }
}