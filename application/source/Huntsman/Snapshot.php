<?php

namespace Huntsman;

use Exception;

include_once(__DIR__ . '/MongoLite.php');

class Snapshot{
  
  const UPLOAD_FOLDER = __DIR__ . "/../../_uploads" . DIRECTORY_SEPARATOR;
  
  public function __construct(){
    $this->db = new MongoLite('photos');
  }
  
  
  /**
   * @param $snapshot
   * @return mixed
   */
  public function saveSnapshot($snapshot){
    return $this->db->save($snapshot);
  }
  
  
  /**
   * @param $imageId
   * @return array
   */
  public function getSingle($imageId){
    return $this->db->getOne($imageId);
  }
  
  
  /**
   * @param null $gallery
   * @param null $limit
   * @return false|mixed|string
   */
  public function getAll($gallery = null, $limit = null){
    return $this->db->getImages($gallery, $limit);
  }
  
  
  /**
   * @param $imageId
   * @return string
   */
  public function remove($imageId){
    $imageDetails = $this->getSingle(intval($imageId));
    
    if ($imageDetails !== "false"){
      $imagePath = self::UPLOAD_FOLDER . $imageDetails["folderName"] . $imageDetails["fileName"];
      
      try{
        if ($this->deleteSnapshot($imagePath) === true)
          return $this->db->removeImage($imageId);
      } catch (Exception $e){
        return $e->getMessage();
      }
    }
  }
  
  
  /**
   * @param $snapshot
   */
  public function storeSnapshot($snapshot){
    if (!file_exists(self::UPLOAD_FOLDER . $snapshot["folderName"])){
      mkdir(self::UPLOAD_FOLDER . $snapshot["folderName"], 0777, true);
    }
    
    move_uploaded_file($snapshot["tmpName"], self::UPLOAD_FOLDER . $snapshot["folderName"] . $snapshot["fileName"]);
  }
  
  
  /**
   * @param $file
   * @return bool
   * @throws Exception
   */
  private function deleteSnapshot($file){
    if (file_exists($file)){
      if (@unlink($file) !== true)
        throw new Exception('Could not delete file: ' . $file . ' Please close all applications that are using it.');
    }
    
    return true;
  }
}