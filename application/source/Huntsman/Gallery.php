<?php

namespace Huntsman;

include_once(__DIR__ . '/MongoLite.php');

class Gallery {
  
  public function __construct(){
    $this->db = new MongoLite('gallery');
  }
  
  
  /**
   * @param $gallery
   * @return mixed
   */
  public function saveGallery($gallery) {
    return $this->db->save($gallery);
  }
  
  
  /**
   * @param $id
   * @return false|mixed|string
   */
  public function getSingle($id) {
    return $this->db->getOne($id);
  }
  
  
  /**
   * @param null $limit
   * @return false|string
   */
  public function getAll($limit = null) {
    return $this->db->getGalleries($limit);
  }
  
}